require('dotenv').config();
const Discord = require('discord.js');
const Gamedig = require('gamedig');
const client = new Discord.Client({ intents: ["GUILDS", "GUILD_MESSAGES"] })

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on('messageCreate', async msg => {

    switch (msg.content) {
        case '!arkse -p':
            try {
                const answer = await qArkse('192.53.175.113')
                msg.channel.send(buildPlayersMsg(answer))
            } catch {
                msg.channel.send('Server Down! 😭😭😭')
            }
            break;
        case '!doe':
            msg.channel.send('คนตอแหล!')
        default:
            break;
    }

});

client.login(process.env.CLIENT_TOKEN);

async function qArkse(host) {
    return await Gamedig.query({
        type: 'arkse',
        host: host
    })
}

function buildPlayersMsg(answer) {
    botMsg = '';
    if (answer.players.length) {
        botMsg = '';
        botMsg += `ตอนนี้มีคนเล่น Ark อยู่ ${answer.players.length} คน 😆 \r\n`
        answer.players.map((player, i) => botMsg += `${i + 1}. ${player.name} --> นั่งเล่นมาแล้ว ${Math.round(player.raw.time / 60)} นาที 👋`).join("\r\n")
    } else {
        botMsg += 'ไม่มีคนอยู่ในเกม Ark 😭😭😭'
    }
    return botMsg
}

